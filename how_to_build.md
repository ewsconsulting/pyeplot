# Build the package:
`
python setup.py sdist bdist_wheel
`
# install the package (for testing):
`
cd dist & pip install --prefix . pyeplot-0.0.1-py3-none-any.whl & cd ..
`

# install the package:
Download the python wheel and run:
`
pip install pyeplot-0.0.1-py3-none-any.whl
`



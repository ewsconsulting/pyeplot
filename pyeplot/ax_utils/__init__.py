#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import uuid

log = logging.getLogger(__name__)


def add_ews_logo(ax, **kwargs):
    kwargs = kwargs.copy()
    try:
        graphics = ax.opts["graphic"]
        if isinstance(graphics, list):
            pass
        ax.opts["graphic"] = [graphics]
    except KeyError:
        ax.opts["graphic"] = []

    style = kwargs.pop("style", {})
    graph = {
        **kwargs,
        "type": "image",
        "style": {"image": "https://www.ews-consulting.com/tl_files/template/layout/logo.gif", **style}
    }
    ax.opts["graphic"].append(graph)
    return ax.opts["graphic"][-1]

#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import uuid

log = logging.getLogger(__name__)


def add_line_zoom0(ax, axis_index=None, throttle_delay=250, datazoom_axis_index=None, x_or_y="x"):
    key = f"{x_or_y}Axis"
    b_id = uuid.uuid4().hex
    if datazoom_axis_index is None:
        datazoom_axis_index = 0

    if axis_index is None:
        axis_index = list(range(len(ax.opts[key]))) if isinstance(
            ax.opts[key], list) else 0

    ax.opts["brush"] = {
        "toolbox": [f'line{x_or_y.upper()}'],
        f"{x_or_y}AxisIndex": axis_index,
        "throttleType": 'debounce',
        "throttleDelay": throttle_delay
    }
    ax.set_toolbox(
        feature_brush_show=True, feature_brush_title_lineX="Horizontally zoom", feature_brush_title_clear="Reset zoom")
    onBrushSelected = ax.add_code(r"""var onBrushSelected_%s = function (ev) {
        if ( ev.batch[0].areas.length == 1) {
            var brushType = ev.batch[0].areas[0].brushType;
            var coordRange = ev.batch[0].areas[0].coordRange;
            this.dispatchAction({ type: 'dataZoom', startValue: coordRange[0], endValue: coordRange[1], dataZoomIndex: %s});
            this.dispatchAction({type: 'axisAreaSelect', intervals: []});
            this.dispatchAction({type: 'brush', command: 'clear', areas: []})
        }     
    };""" % (b_id, datazoom_axis_index.__repr__()))
    onBrushSelected.__name__ = f"onBrushSelected_{b_id}"
    ax.on('brushselected', onBrushSelected)
    return onBrushSelected


def add_line_zoom1(ax, axis_index=None, throttle_delay=250, datazoom_axis_index=None, x_or_y="x"):
    key = f"{x_or_y}Axis"
    b_id = uuid.uuid4().hex
    if datazoom_axis_index is None:
        datazoom_axis_index = 0

    if axis_index is None:
        axis_index = list(range(len(ax.opts[key]))) if isinstance(
            ax.opts[key], list) else 0

    ax.opts["brush"] = {
        "toolbox": [f'line{x_or_y.upper()}'],
        f"{x_or_y}AxisIndex": axis_index,
        "throttleType": 'debounce',
        "throttleDelay": throttle_delay,
        # "throttleType": 'fixRate',
        # "throttleDelay": 250
    }
    ax.set_toolbox(
        feature_brush_show=True, feature_brush_title_lineX="Horizontally zoom", feature_brush_title_clear="Reset zoom")
    return ax.add_event_handler('brushselected', """
    var onBrushEnd_{{ chart.chart_id }} = function () {
        function reset() {
            window.chart = myChart_{{ chart.chart_id }};
            myChart_{{ chart.chart_id }}.dispatchAction({
                type: 'axisAreaSelect',
                intervals: []
            });
            myChart_{{ chart.chart_id }}.dispatchAction({
                type: 'brush',
                command: 'clear',
                areas: areas
            });
            myChart_{{ chart.chart_id }}.dispatchAction({
                type: 'takeGlobalCursor',
                key: 'brush',
                brushOption: {
                    brushType: false,
                    brushMode: 'single'
                }
            });
        }        
        var zooming = false;
        var areas = [];
        return function (ev) {
            window.chart = this
            if (ev.batch[0].areas.length == 1) {
                var brushType = ev.batch[0].areas[0].brushType;
                var coordRange = ev.batch[0].areas[0].coordRange;
                areas = ev.batch[0].areas[0];
                this.dispatchAction({ type: 'dataZoom', startValue: coordRange[0], endValue: coordRange[1], dataZoomIndex:  {{ datazoom_axis_index}} });
                setTimeout(function(){
                    reset();                 
                }, 0.1 * {{ throttle_delay }});
                
                zooming = true;
            } else {
                zooming = false;
                reset();
            }
        } 
    };
    """, "onBrushEnd_{{ chart.chart_id }}()", b_id=b_id, datazoom_axis_index=str(datazoom_axis_index), throttle_delay=throttle_delay)


def add_line_zoom(ax, axis_index=None, throttle_delay=250, datazoom_axis_index=None, x_or_y="x"):
    key = f"{x_or_y}Axis"
    b_id = uuid.uuid4().hex
    if datazoom_axis_index is None:
        datazoom_axis_index = 0

    if axis_index is None:
        axis_index = list(range(len(ax.opts[key]))) if isinstance(
            ax.opts[key], list) else 0

    ax.opts["brush"] = {
        "toolbox": [f'line{x_or_y.upper()}'],
        f"{x_or_y}AxisIndex": axis_index,
    }
    ax.set_toolbox(
        feature_brush_show=True, feature_brush_title_lineX="Horizontally zoom", feature_brush_title_clear="Reset zoom")

    return ax.add_event_handler('brushEnd', """
    var onBrushEnd_{{ chart.chart_id }} = function (params) {        
        if (!params.areas.length) {
            return
        }
        var axisIndex = {{ datazoom_axis_index }};
        var selector = {};
        selector['{{ x_or_y }}' + 'AxisIndex'] = axisIndex;
        var chart = myChart_{{ chart.chart_id }};
        var area = params.areas[0];
        var range = area.range;
        var x = chart.convertFromPixel(selector, range[0]);
        var y = chart.convertFromPixel(selector, range[1]);
        console.log([x, y])
        chart.dispatchAction({
            type: 'axisAreaSelect',
            intervals: []
        });
        chart.dispatchAction({
            type: 'dataZoom',
            startValue: x,
            endValue: y,
        });
        chart.dispatchAction({
            type: 'brush',
            areas: []
        });
    };
    """, "onBrushEnd_{{ chart.chart_id }}", b_id=b_id, datazoom_axis_index=str(datazoom_axis_index), throttle_delay=throttle_delay, x_or_y=x_or_y)


# https://stackoverflow.com/questions/52083551/echarts-js-how-to-programmatically-activate-brush-selection

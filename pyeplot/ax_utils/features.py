#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import uuid

log = logging.getLogger(__name__)


def get_features(ax):
    try:
        return ax.opts["toolbox"]["feature"]
    except Exception as e:
        log.error(e)
    return {}


def set_dataview_visible(ax, visible):
    try:
        get_features(ax)["dataView"]["show"] = visible
    except Exception as e:
        log.error(e)
    return visible

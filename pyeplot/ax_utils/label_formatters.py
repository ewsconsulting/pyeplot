#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import uuid

log = logging.getLogger(__name__)

DEFAULT_DATE_FORMAT = 'dd.MM.yyyy\\nhh:mm'
USE_UTC = "{% set USE_UTC = 'true' if chart._option.get('useUTC', True) else 'false' %}"
DATE_FORMATTER = USE_UTC + "echarts.format.formatTime('%s', index, {{ USE_UTC }})" % DEFAULT_DATE_FORMAT

def make_formatTime_template(**kws):
    fmt = kws.get("fmt", DEFAULT_DATE_FORMAT)
    use_utc = kws.get("use_utc", True)
    value_name = kws.get("value_name", "index")
    if use_utc:
        return USE_UTC + "echarts.format.formatTime('%s', %s, {{ USE_UTC }})" % (fmt, value_name)
    return "echarts.format.formatTime('%s', %s)" % (fmt, value_name)


def make_datetime_formatter(ax, fmt='dd.MM.yyyy hh:mm'):
    return ax.add_js_variable(creation_code="""
    __USE_UTC__
    function datetime_formatter_{{ chart.chart_id }}(params) {   
        {%- if not fmt -%}
        var fmt = '{{ DEFAULT_DATE_FORMAT }}';
        {%- else -%}
        var fmt = '{{ fmt }}';
        {%- endif -%}
        
        var value = params.value ? params.value: params;
        return echarts.format.formatTime(fmt, value, {{ USE_UTC }});
    }""".replace("__USE_UTC__", USE_UTC), alias_code="datetime_formatter_{{ chart.chart_id }}", fmt=fmt, DEFAULT_FORMAT=DEFAULT_DATE_FORMAT)


def make_value_formatter(ax):
    return ax.add_js_variable(creation_code="""
    function value_formatter_{{ chart.chart_id }}(params) {
        {% if chart.opts.schema %}
        var p_schema = option_{{ chart.chart_id }}.schema[params.seriesName];
        var value = params.data[p_schema.dim];
        var real_value = (value - p_schema.offset) * p_schema.r + p_schema.vmin;
        if (p_schema.precision != undefined ) {
            real_value = real_value.toFixed(p_schema.precision);
        }
        {% else %}
        var real_value = params.data[0];
        {% endif %}
        return params.seriesName + ": " + real_value;
    }""", alias_code="value_formatter_{{ chart.chart_id }}")

def make_tooltip_formatter(ax, index_dimension=0, index_name="Date", index_fmt_tpl=None, date_fmt=DEFAULT_DATE_FORMAT, use_utc=True):

    if index_fmt_tpl is None:
        INDEX_FMT = make_formatTime_template(fmt=date_fmt, value_name="index", use_utc=use_utc)
    else: 
        INDEX_FMT = index_fmt_tpl

    return ax.add_js_variable(creation_code="""
    function tooltip_formatter_{{ chart.chart_id }}(paramsList) {
        var index = paramsList[0].data[{{ index_dimension }}];
        var res = "{{ index_name }}: " + __INDEX_FMT__ + "<br>";
        
        {%- if chart.opts.schema -%}
        var schema = option_{{ chart.chart_id }}.schema;
        {%- else -%}
        var schema = {};
        {%- endif -%}

        paramsList.forEach(
            function(params) {
                if (schema[params.seriesName] != undefined ) {
                    var p_schema = schema[params.seriesName];
                    var value = params.data[p_schema.dim];
                    var real_value = (value - p_schema.offset) * p_schema.r + p_schema.vmin;
                    if (p_schema.precision != undefined ) {
                        real_value = real_value.toFixed(p_schema.precision);
                    }
                } else {
                    var real_value = params.data[params.seriesIndex + 1];

                }                
                res += params.seriesName + ": " + real_value + "<br>";
            }
        );
        return res;
    }""".replace("__INDEX_FMT__", INDEX_FMT), alias_code="tooltip_formatter_{{ chart.chart_id }}", index_dimension=index_dimension, index_name=index_name)



def make_axis_label_formatter(ax, fmt="yyyy-MM-dd\\nhh:mm"):
    js_var_name = "formatter_%s" % uuid.uuid4().hex
    return ax.add_js_variable(creation_code=r"""function __js_var_name__(value, index) { 
        {% if chart.opts.get('useUTC', False) %}
            return echarts.format.formatTime('__fmt__', value, true);
        {% else %}
            return echarts.format.formatTime('__fmt__', value, false);
        {% endif %}            
        }""".replace(
            "__js_var_name__", js_var_name
        ).replace(
            "__fmt__", fmt
        ),
        alias_code=js_var_name
    )


def make_axis_pointer_formatter(ax, fmt="yyyy-MM-dd\\nhh:mm"):
    js_var_name = "formatter_%s" % uuid.uuid4().hex
    return ax.add_js_variable(creation_code=
        r"""function __js_var_name__(params) { 
        {% if chart.opts.get('useUTC', False) %}
            return echarts.format.formatTime('__fmt__', params.value, true);
        {% else %}
            return echarts.format.formatTime('__fmt__', params.value, false);
        {% endif %}            
        }""".replace(
            "__js_var_name__", js_var_name
        ).replace(
            "__fmt__", fmt
        ),
        alias_code=js_var_name
    )


def make_label_formatter(ax, fmt="yyyy-MM-dd\\nhh:mm"):
    js_var_name = "formatter_%s" % uuid.uuid4().hex
    return ax.add_js_variable(creation_code=r"""function __js_var_name__(value) { 
        {% if chart.opts.get('useUTC', False) %}
            return echarts.format.formatTime('__fmt__', value, true);
        {% else %}
            return echarts.format.formatTime('__fmt__', value, false);
        {% endif %}            
        }""".replace(
            "__js_var_name__", js_var_name
        ).replace(
            "__fmt__", fmt
        ),
        alias_code=js_var_name
    )


#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import uuid

log = logging.getLogger(__name__)


def add_single_selector_legend(ax, legend_index=0):
    # TODO: Handle multiple legends
    return ax.add_event_handler('legendselectchanged', """    
        var onLegendChanged_{{ chart.chart_id }} = function (ev) {   
            var selected = ev.selected;
            Object.keys(selected).forEach(function(key) {
                selected[key] = key === ev.name;
            });
            myChart_{{ chart.chart_id }}.setOption({ legend: {selected: selected, selectedMode: 'single'} });        
        };
        """, "onLegendChanged_{{ chart.chart_id }}")

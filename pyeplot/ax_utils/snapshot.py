

def add_snapshot_on_render(ax, fname=None, typ="png", pixel_ratio=2, exclude_components=None):
    if exclude_components is None:
        exclude_components = ['toolbox']
    if fname is None:
        fname = "Echart." + typ


    return ax.add_event_handler('finished', """
        var downloaded_{{ chart.chart_id }} = false;    
        function getBrowser() {
            var sBrowser, sUsrAg = navigator.userAgent;
            if (sUsrAg.indexOf("Firefox") > -1) {
            return "Firefox";
            } else if (sUsrAg.indexOf("Opera") > -1 || sUsrAg.indexOf("OPR") > -1) {
            return "Opera";
            } else if (sUsrAg.indexOf("Trident") > -1) {
            return "IE";
            } else if (sUsrAg.indexOf("Edge") > -1) {
            return "Edge";
            } else if (sUsrAg.indexOf("Chrome") > -1) {
            return "Chrome";
            } else if (sUsrAg.indexOf("Safari") > -1) {
            return "Safari";
            }
            return "Unknown";
        }
        var onRenderFinished_{{ chart.chart_id }} = function (ev) {   
            if ( downloaded_{{ chart.chart_id }} )  {
                return ;
            }
            let url = myChart_{{ chart.chart_id }}.getDataURL({
                    type: {{ type | tojson }},
                    pixelRatio: {{ pixel_ratio | tojson }},
                    excludeComponents: {{ exclude_components | tojson}}
                }
            );
            var $a = document.createElement('a');
            $a.download = {{ fname | tojson }};
            $a.target = '_blank';
            $a.href = url;
            let browser = getBrowser();
            console.log(browser);        
            let isNotIE = ( browser !== 'IE' ) && ( browser !== 'Edge' ) && ( browser !== 'Unknown' );
            if ( isNotIE ) {
                var evt = new MouseEvent('click', {
                    view: window,
                    bubbles: true,
                    cancelable: false
                });
                $a.dispatchEvent(evt);
            }
            else {
                if (window.navigator.msSaveOrOpenBlob) {
                    var bstr = atob(url.split(',')[1]);
                    var n = bstr.length;
                    var u8arr = new Uint8Array(n);
                    while (n--) {
                        u8arr[n] = bstr.charCodeAt(n);
                    }
                    var blob = new Blob([u8arr]);
                    window.navigator.msSaveOrOpenBlob(blob, {{ fname | tojson }});
                }
                else {
                    var lang = model.get('lang');
                    var html = ''
                        + '<body style="margin:0;">'
                        + '<img src="' + url + '" style="max-width:100%;" />'
                        + '</body>';
                    var tab = window.open();
                    tab.document.write(html);
                }
            }
            downloaded_{{ chart.chart_id }} = true;
        };
        """, "onRenderFinished_{{ chart.chart_id }}", fname=fname, type=typ, pixel_ratio=pixel_ratio, exclude_components=exclude_components)

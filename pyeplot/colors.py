
#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import re
from collections import namedtuple


import bokeh.colors as b_colors
import bokeh.palettes as b_palettes

import numpy as np
import pandas as pd
from matplotlib.cm import get_cmap, cmap_d
from matplotlib.colors import to_hex


log = logging.getLogger(__name__)  # pylint: disable=invalid-name

HEX_COLOR_RE = re.compile(r'^#([a-fA-F0-9]{3}|[a-fA-F0-9]{6})$')
IntegerRGB = namedtuple('IntegerRGB', ['red', 'green', 'blue'])
PercentRGB = namedtuple('PercentRGB', ['red', 'green', 'blue'])


BOKEH_PALS = [name for name in dir(b_palettes) if isinstance(
    getattr(b_palettes, name), list)]
MPL_PALS = sorted(cmap_d.keys())

PALETTE_NAMES = BOKEH_PALS + [k for k in MPL_PALS if k not in BOKEH_PALS]


def normalize_hex(hex_value):
    """
    Normalize a hexadecimal color value to 6 digits, lowercase.
    """

    match = HEX_COLOR_RE.match(hex_value)
    if match is None:
        raise ValueError(
            "'{}' is not a valid hexadecimal color value.".format(hex_value)
        )
    hex_digits = match.group(1)
    if len(hex_digits) == 3:
        hex_digits = u''.join(2 * s for s in hex_digits)
    return '#{}'.format(hex_digits.lower())


def hex_to_rgb(hex_value):
    """
    Convert a hexadecimal color value to a 3-tuple of integers
    suitable for use in an ``rgb()`` triplet specifying that color.
    """
    hex_value = normalize_hex(hex_value)
    hex_value = int(hex_value[1:], 16)

    return IntegerRGB(
        hex_value >> 16,
        hex_value >> 8 & 0xff,
        hex_value & 0xff
    )


def mpl_palette(name_and_n_colors):
    name_and_n_colors = name_and_n_colors.split("_")
    n_colors = int(name_and_n_colors[-1])
    name = "_".join(name_and_n_colors[:-1])
    cmap = get_cmap(name)
    if cmap is None:
        raise ValueError("{} is not a valid colormap".format(name))
    bins = np.linspace(0, 1, n_colors + 2)[1:-1]
    palette = (
        255.0 * np.array(list(map(np.array, cmap(bins)[:, :3])))).astype("int").tolist()

    def to_hex(x):
        return b_colors.RGB(*x).to_hex()
    return pd.Series([to_hex(i) for i in palette]).unique().tolist()
     


def process_color(name_or_tuple):
    if isinstance(name_or_tuple, str):
        color = getattr(b_colors.named, name_or_tuple, None)
        if not color:
            log.warning(
                f"Color {name_or_tuple!r} is not a named color"
            )
        return str(color)
    elif isinstance(name_or_tuple, tuple):
        color = b_colors.RGB(*name_or_tuple)
        return str(color)
    raise RuntimeError(
        f"Color {name_or_tuple!r} must be either a string or a tuple")


def create_palette(name_or_list):
    if isinstance(name_or_list, str):
        if name_or_list in BOKEH_PALS:
            return getattr(b_palettes, name_or_list)
        try:
            return mpl_palette(name_or_list)
        except ValueError:
            pass
        color = process_color(name_or_list)
        return [color]
    elif isinstance(name_or_list, tuple):
        color = process_color(name_or_list)
        return [color]
    elif isinstance(name_or_list, list):
        return [
            process_color(c) for c in name_or_list
        ]
    raise RuntimeError

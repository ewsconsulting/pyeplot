
import logging

import uuid
import copy
import itertools

# import bokeh.colors
import pandas as pd
import pyecharts as ec  # noqa pep8: disable=E402 pylint: disable=C0413
from jinja2 import Template  # noqa pep8: disable=E402 pylint: disable=C0413
from marshmallow.utils import set_value

from .ax_utils.label_formatters import make_datetime_formatter
from .js_object import JSVariable
from .render_methods import RenderItemFactory
from .colors import PALETTE_NAMES, create_palette

logging.getLogger("macropy.core").setLevel(
    logging.ERROR)  # pylint: disable=invalid-name


log = logging.getLogger(__name__)  # pylint: disable=invalid-name

NULL = ec.NULL  # noqa pylint: disable=E1101



class Constants:
    TOOLBOX_FEATURE = {
        "dataView": {
            "show": True,
            "title": 'Data View',
            "lang": ['Data View', 'Close', 'Refresh']
        },
        "dataZoom": {
            "show": True,
            "title": {
                "zoom": 'Zoom',
                "back": 'Zoom Reset'
            }
        },
        "magicType": {
            "title": {
                "line": 'Switch to Line Chart',
                "bar": 'Switch to Bar Chart',
                "stack": 'Stack',
                "tiled": 'Tile'
            },
            "type": [
                "line",
                "bar",
            ],
            "show": True
        },
        "restore": {
            "show": True,
            "title": 'Restore'
        },
        "saveAsImage": {
            "show": True,
            "title": 'Save as Image',
            "lang": ['Right Click to Save Image']
        },
        "brush": {
            "show": False,
            "title": {
                "rect": 'Box Select',
                "polygon": 'Lasso Select',
                "lineX": 'Horizontally Select',
                "lineY": 'Vertically Select',
                "keep": 'Keep Selections',
                "clear": 'Clear Selections'
            }
        },
    }



class EchartsAxis(ec.Overlap):

    def make_render_method(self, method, **kws):
        return getattr(RenderItemFactory, "make_" + method)(self, **kws)

    PALETTE_NAMES = PALETTE_NAMES

    @classmethod
    def create_palette(cls, *args, **kws):
        return create_palette(*args, **kws)
    NULL = NULL

    def __init__(
            self,
            polar=False,
            minified=True,
            title="",
            subtitle="",
            width="100%",
            height=400,
            title_pos="auto",
            title_top="auto",
            title_color=None,
            subtitle_color=None,
            title_text_size=18,
            subtitle_text_size=12,
            background_color=None,
            page_title=ec.constants.PAGE_TITLE,
            renderer=ec.constants.CANVAS_RENDERER,
            extra_html_text_label=None,
            animation=False,
            palette=None,
            theme=None,
            clean=True,
            date_fmt="yyyy.MM.dd\\nhh:mm"
    ):
        super().__init__(page_title=page_title, width=width, height=height)
        self.date_fmt = date_fmt

        line = ec.Bar(
            title=title,
            subtitle=subtitle,
            is_animation=animation,
            extra_html_text_label=extra_html_text_label,
            renderer=renderer,
            background_color=background_color,
            subtitle_text_size=subtitle_text_size,
            title_text_size=title_text_size,
            subtitle_color=subtitle_color,
            title_color=title_color,
            title_top=title_top,
            title_pos=title_pos,
        )

        line.add("", [], [])
        super().add(line)
        self.theme = theme
        self.minified = minified
        if theme != "ews_theme":
            if palette is not None:
                self.set_palette(palette)
        else:
            self.set_palette(palette)

        self.animation = animation
        self.opts["schema"] = {}
        self.opts["dataset"] = []
        self.opts["series"] = []
        self.opts["toolbox"] = {
            "show": True,
            "orient": "vertical",
            "left": "95%",
            "top": "center",
            "feature": Constants.TOOLBOX_FEATURE
        }

        self.opts["tooltip"] = self.opts["tooltip"]  # ._config
        for ax in ["xAxis", "yAxis"]:
            cfg = self.opts[ax][0]._config
            cfg.pop("data", None)
            cfg["axisLabel"] = cfg["axisLabel"]._config
            self.opts[ax][0] = cfg

        if polar:
            self.opts["polar"] = [{}]
            self.opts["angleAxis"] = self.opts.pop("xAxis", [])
            self.opts["radiusAxis"] = self.opts.pop("yAxis", [])

        self.opts["useUTC"] = True
        self.opts["series"] = []
        self.opts["dataset"] = []
        del self.opts["legend"][0]["data"]
        self.opts["textStyle"] = dict(color='navy', fontFamily="Verdana")
        if clean:
            self.clean_opts()

    def add(self, echart_elem):

        # We need a legend
        try:
            self.opts["legend"][0]
        except KeyError:
            self.opts["legend"] = []
        if "data" not in self.opts["legend"][0]:
            self.opts["legend"][0]["data"] = []
        super().add(echart_elem)

        # We don't want pyecharts.echarts.labels
        self._option = self.clean_up(self.opts)

        # We remove the legend
        if "data" in self.opts["legend"][0]:
            del self.opts["legend"][0]["data"]

        classname = echart_elem.__class__.__name__.lower()
        if classname in ["bar", "line"]:
            l = echart_elem.options["series"][0]["data"]
            axis_size = len(l)
            self.set_xaxis(data=list(range(axis_size)))
            if classname == "bar":
                self.set_xaxis(type="category")

        return self

    @classmethod
    def clean_up(cls, data):
        if isinstance(data, list):
            return [cls.clean_up(v) for v in data]
        elif isinstance(data, dict):
            return {k: cls.clean_up(v) for k, v in data.items()}
        if hasattr(data, "_config"):
            return cls.clean_up(getattr(data, "_config"))
        if hasattr(data, "reason"):
            return data.value
        return data

    def clean_opts(self):
        self._option = self.clean_up(self.opts)
        return self

    def add_yAxis(self):
        self.opts["yAxis"] += [copy.deepcopy(self.opts["yAxis"][-1])]
        return self

    def add_xAxis(self):
        self.opts["xAxis"] += [copy.deepcopy(self.opts["xAxis"][-1])]
        return self

    @classmethod
    def list_palette_names(cls):
        return f"{cls.PALETTE_NAMES!r}"[1:-1].replace("'", "")

    def set_palette(self, name_or_list, override_all=False):
        if name_or_list is None:
            self.opts["color"] = [
                "#003366",
                '#d62728',
                '#ff7f0e',
                '#ffbb78',
                '#2ca02c',
                '#98df8a',
                '#aec7e8',
                '#ff9896',
                '#9467bd',
                '#c5b0d5',
                '#8c564b',
                '#c49c94',
                '#e377c2',
                '#f7b6d2',
                '#7f7f7f',
                '#c7c7c7',
                '#bcbd22',
                '#dbdb8d',
                '#17becf',
                '#9edae5'
            ]
            return self.opts["color"]
        colors = create_palette(name_or_list)
        n_series = len(self.opts["series"])
        n_colors = len(colors)
        if override_all and (n_series > n_colors):
            c_cycle = itertools.cycle(colors)
            colors = [c_cycle.__next__() for _ in range(n_series)]
        self.opts["color"] = colors
        return self.opts["color"]

    # Access to ._option
    @property
    def opts(self):
        return self._option

    # Shortcuts to opts
    @property
    def animation(self):
        return self.opts["animation"]

    @animation.setter
    def animation(self, val):
        self.opts["animation"] = val

    @property
    def toolbox(self):
        return self.opts["toolbox"]

    @property
    def tooltip(self):
        if "tooltip" not in self.opts:
            self.opts["tooltip"] = {}
        return self.opts["tooltip"]

    @property
    def datasets(self):
        if "series" not in self.opts:
            self.opts["dataset"] = []
        return self.opts["dataset"]

    @property
    def series(self):
        if "series" not in self.opts:
            self.opts["series"] = []
        return self.opts["series"]

    # Getters and setters
    def get_title(self):
        return self.opts["title"][0]

    def set_title(self, **kws):
        return self.set(root=self.get_title(), **kws)

    def set_toolbox(self, **kws):
        return self.set(root=self.toolbox, **kws)

    def set_tooltip(self, **kws):
        return self.set(root=self.tooltip, **kws)

    def get_legend(self, index=0):
        return self.opts["legend"][index]

    def set_legend(self, index=0, **kws):
        return self.set(root=self.get_legend(index=index), **kws)

    def get_xaxis(self, index=0):
        return self.opts["xAxis"][index]  # noqa

    def set_xaxis(self, index=0, **kws):
        return self.set(root=self.get_xaxis(index=index), **kws)

    def get_yaxis(self, index=0):
        return self.opts["yAxis"][index]  # noqa

    def set_yaxis(self, index=0, **kws):
        return self.set(root=self.get_yaxis(index=index), **kws)

    def get_angle_axis(self, index=0):
        return self.opts["angleAxis"][index]  # noqa

    def set_angle_axis(self, index=0, **kws):
        return self.set(root=self.get_angle_axis(index=index), **kws)

    def get_radius_axis(self, index=0):
        return self.opts["radiusAxis"][index]  # noqa

    def set_radius_axis(self, index=0, **kws):
        return self.set(root=self.get_radius_axis(index=index), **kws)

    def get_dataset(self, index=0):
        return self.opts["dataset"][index]

    def set_dataset(self, index=0, **kws):
        return self.set(root=self.get_dataset(index=index), **kws)

    def set(self, root=None, **kws):
        if root is None:
            root = self.opts
        for k, v in kws.items():
            self.set_param(k, v, root=root)
        return self

    def set_param(self, param, value, root=None):
        if root is None:
            root = self.opts
        eparams = param.split("_")
        if len(eparams) > 1:
            attr_ord_idx = eparams[0]
            param_name = "_".join(eparams[1:])
            if isinstance(root, (list, tuple)):
                attr_ord_idx = int(attr_ord_idx)
                return self.set_param(param_name, value, root=root[attr_ord_idx])
            else:
                try:
                    new_root = getattr(root, attr_ord_idx, root[attr_ord_idx])
                    new_root = getattr(new_root, "_config", new_root)
                    return self.set_param(param_name, value, root=new_root)
                except KeyError:
                    root[attr_ord_idx] = {}
                    new_root = root[attr_ord_idx]
                    return self.set_param(param_name, value, root=new_root)
        value_name = ".".join(eparams)
        return set_value(root, value_name, value)

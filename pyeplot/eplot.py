#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import tempfile
import time
import uuid

import bokeh.colors
import pandas as pd
import pyecharts as ec  # noqa pep8: disable=E402 pylint: disable=C0413
from jinja2 import Template  # noqa pep8: disable=E402 pylint: disable=C0413

from core.eval.function_creator import list_filenames, remove_filenames
from core.eplot.js_object import JSVariable
from core.eplot.ax_utils.label_formatters import make_datetime_formatter

logging.getLogger("macropy.core").setLevel(
    logging.ERROR)  # pylint: disable=invalid-name


log = logging.getLogger(__name__)  # pylint: disable=invalid-name

NULL = ec.NULL  # noqa pylint: disable=E1101

DATETIME_LABEL_FMT = """
def {{ __func_name__ }}(tooltip):
    value = tooltip.value
    return echarts.format.formatTime('yyyy-MM-dd\\nhh:mm', value)
    date = Date(value)
    return date.toLocaleString('{{ locale }}', { "timeZone": '{{ time_zone }}' })
"""  # https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString


def fixEchartsTZ(x):
    if not hasattr(x.index, "tzinfo"):
        return x
    if x.index.tzinfo is None:
        return x.tz_localize("UTC").tz_convert("Europe/Vienna").tz_localize(None)
    else:
        return x.tz_convert("Europe/Vienna").tz_localize(None)


class EPlotter:

    OPTS = {
        'line': ec.Line,
        'bar': ec.Bar,
        'Scatter': ec.Scatter
    }

    DATETIME_LABEL_FMT = """
    def {{ __func_name__ }}(params):
        return echarts.format.formatTime('yyyy-MM-dd\\nhh:mm', params.value)
    """

    DATETIME_FMT = """
    def {{ __func_name__ }}(value, index):
        return echarts.format.formatTime('yyyy-MM-dd\\nhh:mm', value)
    """

    def __init__(self):
        pass

    list_tempfiles = list_filenames
    remove_tempfiles = remove_filenames

    @classmethod
    def get_marker(cls, **kws):
        kws = kws.copy()
        choices = [
            'rect', 'roundRect', 'triangle', 'diamond',
            'pin', 'arrow', 'emptyCircle', 'circle'
        ]
        res = dict(
            symbol=kws.pop("marker", "none"),
            symbol_size=kws.pop("markersize", 4),
            is_symbol_show=kws.pop("showmarker", True),
        )

        res["is_symbol_show"] = (
            res["symbol"] != "none") and res["is_symbol_show"]
        return res

    @classmethod
    def get_legend(cls, **kws):
        kws = kws.copy()
        res = dict(
            is_legend_show=kws.pop("legend", False),
            legend_orient=kws.pop("legend_orient", None),
            legend_pos=kws.pop("legend_pos", None),
            legend_top=kws.pop("legend_top", None),
            legend_selectedmode=kws.pop("legend_selectedmode", None),
            legend_text_size=kws.pop("legend_text_size", None),
            legend_text_color=kws.pop("legend_text_color", None),
        )
        return res

    @classmethod
    def get_method_for_kind(cls, kind):
        kind = kind.title()
        try:
            method = getattr(ec, kind)
        except AttributeError:
            raise AttributeError(
                f"Plot of kind {kind!r} is not supported"
            )
        return method

    @classmethod
    def setup_figure(cls, **kws):
        kws = kws.copy()
        figsize = kws.pop("figsize", ("80%", 400))
        return dict(
            width=figsize[0],
            height=figsize[1],
        ), kws

    @classmethod
    def get_xlim(cls, **kws):
        xlim = kws.pop("xlim", ("dataMin", "dataMax"))
        return dict(
            xaxis_min=xlim[0],
            xaxis_max=xlim[1],
            xaxis_force_interval=kws.pop("xspacing", NULL)
        )

    @classmethod
    def get_ylim(cls, **kws):
        ylim = kws.pop("ylim", ("dataMin", "dataMax"))
        return dict(
            yaxis_min=ylim[0],
            yaxis_max=ylim[1],
            yaxis_force_interval=kws.pop("yspacing", NULL)
        )

    @classmethod
    def get_line_opts(cls, **kws):
        color = kws.pop("color", None)
        linecolor = kws.pop("linecolor", None)
        if not color:
            color = linecolor
        try:
            color = getattr(
                bokeh.colors.named,
                color
            ).to_hex()
        except Exception as _:
            pass

        return color, dict(
            line_type=kws.pop("linestyle", "solid"),
            line_opacity=kws.pop("opacity", 1),
            line_width=kws.pop("linewidth", 1),
            line_color=color,
        )

    @classmethod
    def get_bar_opts(cls, **kws):
        color = kws.pop("color", None)
        try:
            color = getattr(
                bokeh.colors.named,
                color
            ).to_hex()
        except Exception as e:
            # log.warning(e)
            pass

        return color, dict(
            barBorderColor=kws.pop("edgecolor", NULL),
            barBorderWidth=kws.pop("edgewidth", NULL),
            barGap=kws.pop("gap", NULL),
            barWidth=kws.pop("width", NULL),
            color=color,
        )

    @classmethod
    def plot_serie(cls, s, **kws):
        kws = kws.copy()
        if kws.pop("fix_tz", False):
            s = fixEchartsTZ(s)

        plot = kws.pop("ax", None)
        useUTC = kws.pop("useUTC", True)
        date_fmt = kws.pop("date_fmt", "yyyy.MM.dd\\nhh:mm")
        plot_kws, kws = cls.setup_figure(**kws)
        if plot is None:
            plot = ec.Overlap(
                **plot_kws
            )
            plot.theme = "ews_theme"

        kind = kws.pop("kind", "line").title()
        plot_method = cls.get_method_for_kind(kind)
        title = kws.pop("title", "")
        suptitle = kws.pop("suptitle", "")
        name = s.name if isinstance(s.name, str) else s.name.__repr__()
        label = kws.pop("label", name)

        xaxis_type = "log" if kws.pop(
            "logx", None) else kws.pop("xaxis_type", "value")
        yaxis_type = "log" if kws.pop(
            "logy", None) else kws.pop("yaxis_type", "value")
        xaxis_type = "time" if isinstance(
            s.index, pd.DatetimeIndex
        ) else xaxis_type
        kws.update(
            xaxis_type=xaxis_type,
            yaxis_type=yaxis_type,
            **cls.get_xlim(**kws),
            **cls.get_ylim(**kws)
        )
        xaxis_formatter = None
        if xaxis_type == "time":
            xaxis_formatter = plot.make_datetime_formatter(date_fmt)

        opts = {}
        if kind == "Line":
            color, opts = cls.get_line_opts(**kws)

        marker_opts = cls.get_marker(**kws)
        legend_opts = cls.get_legend(**kws)

        later_kws = dict(
            symbolRotate=kws.pop("symbol_rotate", 0),
            connectNulls=kws.pop("connect_nulls", False),
            # [True, False, 'start', 'middle', 'end']
            step=kws.pop("step", False),
            zlevel=kws.pop("zlevel", 0),
            animation=kws.pop("animation", False),
            # ['average', 'max', 'min', 'sum', None]
            sampling=kws.pop("sampling", 'average'),
            large=kws.pop("large", False),
        )

        if kind == "Bar":
            color, opts = cls.get_bar_opts(**kws)
            later_kws.update(opts)

        kws.update(
            datazoom_type=kws.pop("datazoom_type", "both"),
            is_datazoom_show=kws.pop("datazoom_show", True),
            datazoom_range=kws.pop("datazoom_range", [0, 100]),
            tooltip_trigger=kws.pop("tooltiptrigger ", "axis"),
            tooltip_trigger_on=kws.pop("tooltiptriggeron", "mousemove|click"),
            tooltip_axispointer_type=kws.pop(
                "tooltip_axispointer_type", "cross"),
            is_toolbox_show=kws.pop("is_toolbox_show", False),
            is_more_utils=kws.pop("is_more_utils", False),
            **opts,
            **marker_opts,
            **legend_opts,
        )
        # log.error(kws)
        chart = plot_method(
            title, suptitle
        )
        chart.add(
            label,
            s.index,
            s.values,
            **kws
        )
        chart._option["useUTC"] = useUTC
        opts = chart._option["series"][-1]
        # log.error(opts['lineStyle']['normal']["color"])

        opts.update(later_kws)
        plot.add(chart)

        def get_new_id():
            plot._chart_id = uuid.uuid4().hex
            return plot.chart_id
        plot.get_new_id = get_new_id

        if xaxis_formatter is not None:
            plot._option["xAxis"][0]["axisLabel"]["formatter"] = xaxis_formatter
            plot._option["xAxis"][0]["axisPointer"] = {
                "show": True, "label": {}}
            plot._option["xAxis"][0]["axisPointer"]["label"]["formatter"] = xaxis_formatter
        return plot

    @classmethod
    def plot_dataframe(cls, df, **kws):
        kws = kws.copy()
        plot = kws.pop("ax", None)
        features = kws.pop("features", ['dataZoom', 'restore', 'saveAsImage'])
        if kws.pop("fix_tz", False):
            df = fixEchartsTZ(df)
        palette = kws.pop("palette", None)

        plot_kws, kws = cls.setup_figure(**kws)
        if plot is None:
            plot = ec.Overlap(
                **plot_kws
            )

        columns = kws.pop("columns", df.columns.tolist())
        kws["ax"] = plot
        for c in columns:
            cls.plot_serie(
                df[c], **kws
            )
        if palette is not None:
            plot._option["color"] = palette

        plot._option["toolbox"] = {'show': True,
                                   'orient': 'vertical',
                                   'left': '95%',
                                   'top': 'center',
                                   'feature': {'dataView': {'show': True,
                                                            'title': 'Data View',
                                                            'lang': ['Data View', 'Close', 'Refresh']},
                                               'dataZoom': {'show': True, 'title': {'zoom': 'Zoom', 'back': 'Zoom Reset'}},
                                               'magicType': {'title': {'line': 'Switch to Line Chart',
                                                                       'bar': 'Switch to Bar Chart',
                                                                       'stack': 'Stack',
                                                                       'tiled': 'Tile'},
                                                             'type': ['line', 'bar'],
                                                             'show': True},
                                               'restore': {'show': True, 'title': 'Restore'},
                                               'saveAsImage': {'show': True,
                                                               'title': 'Save as Image',
                                                               'lang': ['Right Click to Save Image']},
                                               'brush': {'show': True,
                                                         'title': {'rect': 'Box Select',
                                                                   'polygon': 'Lasso Select',
                                                                   'lineX': 'Horizontally zoom',
                                                                   'lineY': 'Vertically Select',
                                                                   'keep': 'Keep Selections',
                                                                   'clear': 'Reset zoom'}}}}
        for k, v in plot._option["toolbox"]["feature"].items():
            v.update(show=k in features)
        return plot

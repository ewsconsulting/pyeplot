#! /usr/bin/env python
# -*- coding: utf-8 -*-


import logging
import os
import subprocess
import tempfile

import requests

try:
    # !pip install rJSmin
    from rjsmin import jsmin  # pylint: disable=invalid-name
    HAS_JSMIN = True
except ImportError:
    HAS_JSMIN = False

try:
    from css_html_js_minify import html_minify  # pylint: disable=invalid-name
    HAS_HTML_MINIFY = True
except ImportError:
    HAS_HTML_MINIFY = False

try:
    from bs4 import BeautifulSoup  # pylint: disable=invalid-name
    HAS_BEAUTIFUL_SOUP = True
except ImportError:
    HAS_BEAUTIFUL_SOUP = False
log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def minify(html, engine="jsmin"):
    engine = engine.lower()
    #log.debug(f"Minifying html using {engine}")
    if (engine == "jsmin") and HAS_JSMIN:
        html = jsmin(html)
    elif engine == "html-minifier":
        with tempfile.NamedTemporaryFile(mode="w+", delete=False) as temp:
            temp.write(html)
            temp.flush()
            #!npm install html-minifier -g
            cmd = "html-minifier --collapse-whitespace --remove-comments --remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes --remove-tag-whitespace --use-short-doctype --minify-css true --minify-js true"
            html = subprocess.check_output(
                cmd + f" {temp.name}",
                shell=True, encoding="utf-8"
            )
        os.remove(temp.name)
        return html
    elif engine == "online":
        try:
            url = 'https://html-minifier.com/raw'
            response = requests.post(url, {'input': html})
            return response.text
        except:  # noqa pylint: disable=W0702
            return html
    elif (engine == "html_minify") and HAS_HTML_MINIFY:
        html = html_minify(html, comments=True)
    return html


def prettify(html, encoding=None, formatter='minimal'):
    if HAS_BEAUTIFUL_SOUP:
        html = BeautifulSoup(html, "html.parser").prettify(
            encoding=encoding, formatter=formatter)
    return html

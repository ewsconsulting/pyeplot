#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from jinja2 import Template

log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class JSVariable:

    @property
    def rendered(self):
        return len(self._results.keys()) > 0

    @property
    def __name__(self):
        return self.render()._results.get("alias", None)  # noqa pylint: disable=protected-access

    def __init__(self, creation_code=None, alias_code=None, **context):
        self._codes = dict(
            creation=creation_code,
            alias=alias_code,
        )
        self._templates = dict()
        self._results = dict()
        self.context = context

    def create_templates(self):
        # print("create_templates")
        for key, code in self._codes.items():
            if code is not None:
                self._templates[key] = Template(code)
        return self

    def render(self):
        # print("render")
        self.create_templates()
        for k, v in self.context.items():
            for key, tpl in self._templates.items():
                tpl.globals[k] = v
        for key, tpl in self._templates.items():
            self._results[key] = tpl.render().strip()
        return self

    def __str__(self):
        return (
            f"<{self.__class__.__name__} "
            f"\ncreation={self._codes['creation'].strip()} "
            f"\nalias={self._codes['alias'].strip()!r} "
            f"\nvariables={list(self.context.keys())!r} "
            f"/>"
        )

    def __repr__(self):
        return self.__str__()

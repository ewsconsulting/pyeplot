import os
import io
from jinja2 import Markup, environmentfunction


@environmentfunction
def add_js_utils(env):
    dir_name = os.path.split(__file__)[0]
    code_fname = os.path.join(
        dir_name,
        "js_utils.js",
    )
    code = ""
    with io.open(code_fname, "r") as fin:
        code = fin.read()
    return Markup(code)

#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging

log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def patch_module_or_class(module_or_class, func, is_callable=True, name=None, on_class=False):
    if name is None:
        name = func.__name__

    if is_callable:
        if not on_class:
            class Wrapper:
                def __init__(self, _data):
                    self._data = _data

                def __call__(self, *args, **kws):
                    return func(self._data, *args, **kws)

            Wrapper.__call__.__doc__ = func.__doc__

            def _patch(self):
                return Wrapper(self)

            setattr(module_or_class, name, property(_patch))
        else:
            setattr(module_or_class, name, func)

        getattr(module_or_class, name).__doc__ = func.__doc__
        return True
    if not on_class:
        setattr(module_or_class, name, property(func))
    else:
        setattr(module_or_class, name, func)

    getattr(module_or_class, name).__doc__ = func.__doc__
    return True


def patch_modules_or_classes(module_or_classes, func, is_callable=True, name=None, on_class=False):

    if isinstance(module_or_classes, (list, tuple)):
        res = list()
        for module_or_class in module_or_classes:
            res += [patch_module_or_class(module_or_class, func,
                                          is_callable=is_callable, name=name, on_class=on_class)]
        return all(res)
    elif isinstance(module_or_classes, str):
        return patch_module_or_class(module_or_class, func,
                                     is_callable=is_callable, name=name, on_class=on_class)
    else:
        raise RuntimeError


def patched(**argument):
    def real_decorator(function):
        name = argument.pop("name", function.__name__)
        classes = argument.pop("classes", None)
        if classes is None:
            return function
        patch_modules_or_classes(classes, function, name=name, **argument)
        return function
    return real_decorator

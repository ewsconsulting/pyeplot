#! /usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=protected-access,E0202,W0212,W0221,W0703,C0412

import datetime
import json
import logging
import os
import subprocess
import tempfile
import types
import uuid
from collections import OrderedDict

import pandas as pd
import pandas.io.parsers
from jinja2 import Template

import pyecharts as ec  # noqa pep8: disable=E402 pylint: disable=C0413
import requests
from marshmallow.utils import missing

from pyecharts.engine import (ECHAERTS_TEMPLATE_FUNCTIONS, FUNCTION_TRANSLATOR,
                              TRANSLATOR)
from pyecharts_javascripthon.api import FunctionSnippet, JavascriptSnippet
from pyecharts_javascripthon.compat import TranslatorCompatAPI

from .html_utils import minify, prettify
from .js_object import JSVariable
from .js_utils import add_js_utils
from .monkey_patching import patched

HAS_GEOPANDAS = False
try:
    import geopandas as gpd
    HAS_GEOPANDAS = True
except ImportError:
    pass



# ----- Add custom javascript functions (as Jinja2 template functions) ----- #
ECHAERTS_TEMPLATE_FUNCTIONS.update(add_js_utils=add_js_utils)



log = logging.getLogger(__name__)  # pylint: disable=invalid-name

PANDAS_OBJECTS = [
    pd.Series,
    pd.io.parsers.Series,
    pd.core.series.Series,
    pd.DataFrame,
    pd.io.parsers.DataFrame,
    pd.core.frame.DataFrame,
]
if HAS_GEOPANDAS:
    PANDAS_OBJECTS += [gpd.GeoDataFrame, gpd.GeoSeries]
    



TRANSLATOR.reference_str_format = '-=>{name}<=-'
JS_HOST = "https://cdn.jsdelivr.net/npm/echarts/dist/"
# JS_HOST = "https://cdnjs.cloudflare.com/ajax/libs/echarts/4.2.1/"
ec.online(host=JS_HOST)
ec.configure(
    # echarts_template_dir=str(PATHS.lib.joinpath("core").joinpath("eplot").joinpath("templates")),
    echarts_template_dir=os.path.join(os.path.split(__file__)[0], "templates"),
    hosted_on_github=False,
    jshost=JS_HOST
)



log.warning(
    "Patching Echarts for minification"
)

ec.base.Base.date_format = r"yyyy-MM-dd\nhh:mm"

@patched(classes=[ec.base.Base], is_callable=True, name="_repr_html_")
def _repr_html_(self):
    self._chart_id = uuid.uuid4().hex
    self._option["__handlers__"] = {
        k: v for k, v in self.event_handlers.items() if hasattr(v, 'create_templates')}

    if ec.conf.CURRENT_CONFIG.jupyter_presentation == ec.constants.DEFAULT_HTML:
        require_config = ec.conf.CURRENT_CONFIG.produce_require_configuration(
            self.js_dependencies
        )
        config_items = require_config["config_items"]
        libraries = require_config["libraries"]
        env = ec.engine.create_default_environment(ec.constants.DEFAULT_HTML)
        html = env.render_chart_to_notebook(
            charts=(
                self,), config_items=config_items, libraries=libraries, template_name="notebook.html.j2"
        )

    elif ec.conf.CURRENT_CONFIG.jupyter_presentation == ec.constants.NTERACT:
        env = ec.engine.create_default_environment(ec.constants.DEFAULT_HTML)
        html = env.render_chart_to_notebook(
            chart=self, template_name="nteract.html"
        )

    else:
        html = None

    if html is not None:
        html = html.replace("echarts.min.js", "echarts-en.min.js")
        if getattr(self, "minified", True):
            html = minify(html)
        else:
            if getattr(self, "pretty", True):
                html = prettify(html)
    return html


@patched(classes=[ec.base.Base], is_callable=True, name="write_html")
def write_html(self, filename, minified=None, show_gui=None, pretty=None):
    self._option["__handlers__"] = {
        k: v for k, v in self.event_handlers.items() if hasattr(v, 'create_templates')}
    page_title = getattr(self, "_page_title", "Echarts")
    is_file_handler = False
    if page_title == "Echarts":
        try:
            self._page_title = os.path.basename(os.path.splitext(filename)[0])
            is_file_handler = True
        except TypeError:
            pass
    minified = getattr(
        self, "minified", True) if minified is None else minified
    pretty = getattr(
        self, "pretty", True) if pretty is None else pretty

    my_show_gui = getattr(self, "show_gui", None)
    show_gui = my_show_gui if show_gui is None else show_gui
    if show_gui is None:
        show_gui = False

    _engine = ec.engine.EchartsEnvironment(
        pyecharts_config=ec.conf.CURRENT_CONFIG
    )
    tpl = _engine.get_template('simple_chart.html.j2')
    require_config = ec.conf.CURRENT_CONFIG.produce_require_configuration(
        self.js_dependencies
    )
    config_items = require_config["config_items"]
    libraries = require_config["libraries"]
    self.show_gui = show_gui
    try:
        html = tpl.render(
            chart=self, config_items=config_items, libraries=libraries)
        html = html.replace("echarts.min.js", "echarts-en.min.js")
    finally:
        self.show_gui = my_show_gui
        self._page_title = page_title
    if minified:
        html = minify(html)
    else:
        if pretty:
            html = prettify(html)
    if is_file_handler:
        ec.utils.write_utf8_html_file(filename, html)
    else:
        filename.write(html)
    return self


@patched(classes=[ec.base.Base], is_callable=True, name="to_iframe")
def to_iframe(self, height=None, display=True):
    import html
    import re
    import io
    stream = io.StringIO()
    self.write_html(stream, minified=True)
    htmlcontent = stream.getvalue()
    stream.close()
    htmlsrcdoc = html.escape(htmlcontent)
    #htmlsrcdoc = re.sub('\\n', ' ', htmlsrcdoc)
    #htmlsrcdoc = re.sub(' +', ' ', htmlsrcdoc)
    width = self.width
    if not height:
        height = self.height
        try:
            height = int(str(height).replace("px", "")) + 15
        except:
            pass
    code = '<iframe style="border:0;outline:0;overflow:hidden;" srcdoc="' + \
        htmlsrcdoc + '" height=' + \
        str(height) + ' width=' + str(width) + '></iframe>'
    if display:
        from IPython.display import display, HTML
        display(HTML(code))
        return self
    return code


@patched(classes=[ec.base.Base], is_callable=False, name="iframe")
def iframe(self):
    import html
    import re
    import io
    stream = io.StringIO()
    self.write_html(stream, minified=True)
    htmlcontent = stream.getvalue()
    stream.close()
    htmlsrcdoc = html.escape(htmlcontent)
    #htmlsrcdoc = re.sub('\\n', ' ', htmlsrcdoc)
    #htmlsrcdoc = re.sub(' +', ' ', htmlsrcdoc)
    width = self.width
    height = self.height
    if getattr(self, "height_offset", False):
        try:
            height = int(str(height).replace("px", "")) + \
                int(str(self.height_offset).replace("px", ""))
        except:
            pass
    else:
        try:
            height = int(str(height).replace("px", "")) + 15
        except:
            pass

    return '<iframe style="border:0;outline:0;overflow:hidden;" srcdoc="' + htmlsrcdoc + '" height=' + str(height) + ' width=' + str(width) + '></iframe>'


@patched(classes=[ec.base.Base], is_callable=False, name="display")
def _display(self):
    from IPython.display import display, HTML
    display(HTML(self.iframe))
    return self

@patched(classes=[ec.base.Base], is_callable=True, name="add_js_variable")
def add_js_variable(self, variable=None, creation_code=None, alias_code=None, **context):
    context["chart"] = self
    self.__class__.JSVariable = JSVariable
    if variable is not None:
        variable.context.update(context)
        return variable
    return JSVariable(
        creation_code=creation_code,
        alias_code=alias_code,
        **context
    )


@patched(classes=[ec.base.Base], is_callable=True, name="add_init_code")
def add_init_code(self, creation_code=None, alias_code=None, **context):
    init_code = self._option.pop("init_code", [])
    jsVar = None
    try:
        jsVar = self.add_js_variable(
            creation_code=creation_code, alias_code=alias_code, **context)
        init_code.append(jsVar)
    finally:
        self._option["init_code"] = init_code
    return jsVar


@patched(classes=[ec.base.Base], is_callable=True, name="add_line_zoom")
def add_line_zoom(self, *args, **kws):
    from .ax_utils.brush import add_line_zoom  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return add_line_zoom(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="set_dataview_visible")
def set_dataview_visible(self, *args, **kws):
    from .ax_utils.features import set_dataview_visible  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return set_dataview_visible(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_formatTime_template")
def make_formatTime_template(self, **kws):  # noqa pylint: disable=unused-argument
    from .ax_utils.label_formatters import make_formatTime_template  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_formatTime_template(**kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_datetime_formatter")
def make_datetime_formatter(self, *args, **kws):
    from .ax_utils.label_formatters import make_datetime_formatter  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_datetime_formatter(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_value_formatter")
def make_value_formatter(self, *args, **kws):
    from .ax_utils.label_formatters import make_value_formatter  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_value_formatter(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_tooltip_formatter")
def make_tooltip_formatter(self, *args, **kws):
    from .ax_utils.label_formatters import make_tooltip_formatter  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_tooltip_formatter(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_axis_label_formatter")
def make_axis_label_formatter(self, *args, **kws):
    from .ax_utils.label_formatters import make_axis_label_formatter  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_axis_label_formatter(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_axis_pointer_formatter")
def make_axis_pointer_formatter(self, *args, **kws):
    from .ax_utils.label_formatters import make_axis_pointer_formatter  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_axis_pointer_formatter(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="make_label_formatter")
def make_label_formatter(self, *args, **kws):
    from .ax_utils.label_formatters import make_label_formatter  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return make_label_formatter(self, *args, **kws)


@patched(classes=[ec.base.Base], is_callable=True, name="add_event_handler")
def add_event_handler(self, event_name, creation_code, alias_code, **context):
    handler = self.add_js_variable(
        creation_code=creation_code, alias_code=alias_code, **context)
    self.event_handlers[event_name] = handler
    return handler


@patched(classes=[ec.base.Base], is_callable=True, name="add_single_selector_legend")
def add_single_selector_legend(ax, *args, **kws):
    from .ax_utils.legend import add_single_selector_legend  # noqa pylint: disable=wrong-import-position,redefined-outer-name
    return add_single_selector_legend(ax, *args, **kws)



@patched(classes=[ec.base.Base], is_callable=True, name="as_ec")
def as_ec(self, reset=True):
    from .echartsAxis import EchartsAxis
    ax = EchartsAxis()
    ax._option = self._option
    ax.clean_opts()
    if reset:
        ax.set(tooltip={})
    return ax



class DefaultJsonEncoder(json.JSONEncoder):

    def default(self, obj):

        if isinstance(obj, types.FunctionType):
            return FUNCTION_TRANSLATOR.feed(obj)

        if isinstance(obj, (datetime.datetime, datetime.date)):
            return obj.isoformat()

        func_code = getattr(obj, "_func_code", None)
        js_var_name = getattr(obj, "js_var_name", None)
        if func_code is not None:
            name = f"__custom_func_{id(obj)}"
            code = obj.render()
            if js_var_name and (js_var_name in code):
                name = js_var_name
            FUNCTION_TRANSLATOR._func_store.update({name: obj})
            ref_str = FUNCTION_TRANSLATOR.reference_str_format.format(
                name=name)
            new_name = ''.join(['"', ref_str, '"'])
            if new_name in [i[0] for i in FUNCTION_TRANSLATOR._replace_items]:
                return ref_str

            FUNCTION_TRANSLATOR._replace_items.append((new_name, code))
            return ref_str

        if hasattr(obj, 'config'):
            return obj.config

        if getattr(obj, 'create_templates', missing) is not missing:
            # rendered = getattr(obj, 'rendered', False)
            obj.render()
            # if not rendered:
            #    obj.render()
            creation_results = obj._results.get("creation", missing)
            alias_results = obj._results.get("alias", missing)
            if creation_results is not missing:
                code = creation_results.strip()
                ref_str = TRANSLATOR.reference_str_format.format(
                    name=f"{id(obj)}_create")
                key = ''.join(['"', ref_str, '"'])
                if alias_results is missing:
                    TRANSLATOR.lut[key] = code
                    return ref_str
                else:
                    if key not in TRANSLATOR.lut.keys():
                        TRANSLATOR.function_snippet += key + "\n"
                        TRANSLATOR.lut[key] = code
            if alias_results is not missing:
                code = alias_results.strip()
                ref_str = TRANSLATOR.reference_str_format.format(
                    name=f"{id(obj)}_alias")
                key = ''.join(['"', ref_str, '"'])
                TRANSLATOR.lut[key] = code
                return ref_str

        try:
            return obj.tolist()
        except Exception:
            return super(DefaultJsonEncoder, self).default(obj)


def translate():
    fs = FunctionSnippet()
    for name, func in FUNCTION_TRANSLATOR._func_store.items():
        if name in FUNCTION_TRANSLATOR._shared_function_snippet:
            snippet = FUNCTION_TRANSLATOR._shared_function_snippet.get_snippet(
                name)
        else:
            is_custom = hasattr(func, "_func_code")
            if not is_custom:
                snippet = TranslatorCompatAPI.translate_function(func)
            else:
                snippet = func._results
            FUNCTION_TRANSLATOR._shared_function_snippet.add(snippet, name)
        fs.add(snippet, name)
    # log.error(FUNCTION_TRANSLATOR._replace_items)
    return fs


def translate2(options):

    option_snippet = json.dumps(
        options, indent=None, cls=TRANSLATOR.json_encoder, separators=(',', ':'))

    function_snippet = FUNCTION_TRANSLATOR.translate()
    lut = {}
    if function_snippet:
        lut = dict(zip(function_snippet._function_codes,
                       function_snippet._function_names))
    for src, dest in FUNCTION_TRANSLATOR._replace_items:
        is_anonymous = src.startswith('"-=>__custom_func_')
        if not is_anonymous:
            dest = lut.get(dest, dest)
        option_snippet = option_snippet.replace(src, dest)

    return JavascriptSnippet(function_snippet.as_snippet(), option_snippet)


def translate_new(options):
    TRANSLATOR.lut = OrderedDict()
    TRANSLATOR.function_snippet = ""
    init_code = options.pop("init_code", [])
    for obj in init_code:
        json.dumps(
            obj, indent=None, cls=TRANSLATOR.json_encoder, separators=(',', ':')
        )

    option_snippet = json.dumps(
        options, indent=None, cls=TRANSLATOR.json_encoder, separators=(',', ':'))

    if init_code:
        options["init_code"] = init_code
    function_snippet = TRANSLATOR.function_snippet
    for pattern, code in TRANSLATOR.lut.items():
        function_snippet = function_snippet.replace(pattern, code)
        option_snippet = option_snippet.replace(pattern, code)

    TRANSLATOR.lut = OrderedDict()
    TRANSLATOR.function_snippet = ""
    return JavascriptSnippet(function_snippet, option_snippet)


FUNCTION_TRANSLATOR.translate = translate
TRANSLATOR.translate = translate_new
TRANSLATOR.json_encoder = DefaultJsonEncoder


def feed(func, name=None):
    func_code = getattr(func, "_func_code", None)
    if func_code is not None:
        name = f"custom_func_{id(func)}"
        FUNCTION_TRANSLATOR._func_store.update({name: func})
        ref_str = FUNCTION_TRANSLATOR.reference_str_format.format(
            name=name)
        FUNCTION_TRANSLATOR._replace_items.append(
            (''.join(['"', ref_str, '"']), func.render()))
        return ref_str
    name = name or func.__name__
    FUNCTION_TRANSLATOR._func_store.update({name: func})
    ref_str = FUNCTION_TRANSLATOR.reference_str_format.format(name=name)
    FUNCTION_TRANSLATOR._replace_items.append(
        (''.join(['"', ref_str, '"']), name))
    return ref_str


FUNCTION_TRANSLATOR.feed = feed



log.warning(
    "Patching pandas for Eplot"
)


PANDAS_OBJECTS = [
    pd.Series,
    pd.io.parsers.Series,
    pd.core.series.Series,
    gpd.GeoSeries,
    pd.DataFrame,
    pd.io.parsers.DataFrame,
    pd.core.frame.DataFrame,
    gpd.GeoDataFrame]


@patched(classes=PANDAS_OBJECTS, is_callable=True, name="to_eplot")
def to_eplot(df, **kws):

    flip_axes = kws.pop("flip_axes", False)
    kws["clean"] = True
    dtdict = {
        "datetime64[ns]": "time",
        "datetime64[ns, UTC]": "time",
        "float64": "float",
        "float32": "float",
        "float": "float",
        "int64": "int",
        "int32": "int",
        "int": "int",
        "object": "ordinal",
    }
    from .echartsAxis import EchartsAxis
    kind = kws.pop("kind", "line")
    palette = kws.pop("palette", None)
    use_utc = kws.pop("use_utc", True)
    xlim = kws.pop("xlim", (None, None))
    ylim = kws.pop("ylim", (None, None))
    linestyle = kws.pop("linestyle", dict(type="solid", width=2))

    color = None
    if isinstance(palette, list):
        color = list(palette)
    else:
        kws["palette"] = palette

    data = df.rename_axis("__index__").reset_index()
    date_fmt = kws.pop("date_fmt", 'dd.MM.yyyy\\nhh:mm')
    pydate_fmt = kws.pop("pydate_fmt", '%Y.%m.%d %H:%M')

    features = kws.pop("features", ['dataZoom', 'restore', 'saveAsImage'])

    x = kws.pop("x", "index")
    cols = ["__index__"]
    av_cols = data.columns
    if x != "index":
        assert x in av_cols, f"x must be in {av_cols.tolist()!r}"
        cols = [x]

    av_cols = [c for c in av_cols]
    y = kws.pop("y", None)
    if y is None:
        y = [c for c in av_cols]
    else:
        if not isinstance(y, list):
            y = [y]
    y = [c for c in y if (c in av_cols) and (c != cols[0])]
    cols += y

    tooltip = kws.pop("tooltip", None)
    if tooltip is None:
        tooltip = [c for c in av_cols]
    else:
        if not isinstance(tooltip, list):
            tooltip = [tooltip]

    tooltip = [c for c in tooltip if c in av_cols]

    cols += [c for c in y if c not in cols]
    cols += [c for c in tooltip if c not in cols]

    # By
    by = kws.pop("by", None)
    cmap = kws.pop("cmap", None)
    if by in av_cols:
        if by not in cols:
            cols += [by]
    cols = pd.Series(cols).drop_duplicates().tolist()

    data = data[cols]
    dtypes = data.dtypes.astype("str").apply(dtdict.get).rename_axis(
        "name").rename("type").to_frame().reset_index()
    dtypes["displayName"] = [f"{c}" for c in data.columns]
    if "__index__" in cols:
        dtypes.loc[cols.index("__index__"), "displayName"] = str(df.index.name)
    dimensions = dtypes.to_dict(orient="records")
    # data.head(10).idisplay;
    for i, row in dtypes.iterrows():
        name = row["name"]
        if row["type"] == "time":
            if (name == cols[0]) or (name == by):
                data[name] = (pd.DatetimeIndex(data[name]).astype("int64") * 1e-6)
            else:
                data[name] = pd.DatetimeIndex(
                    data[name]).strftime(pydate_fmt).values
                dimensions[i]["type"] = "ordinal"
    ds = {"source": data.values, "dimensions": dimensions}

    ax = kws.pop("ax", None)

    series_kws = kws.pop("series_kws", {})
    legend = kws.pop("legend", len(y) > 1)
    if ax is None:
        ax = EchartsAxis(**kws)
    ax.opts["useUTC"] = use_utc
    ax.opts["dataset"] += [ds]

    for yc in y:
        ax.opts["series"] += [{
            "datasetIndex": len(ax.opts["dataset"]) - 1,
            "name": yc if legend else None,
            "type": kind,
            "showSymbol": True,
            "symbolSize": 4,
            "encode": {
                "x": x if not flip_axes else yc,
                "y": yc if not flip_axes else x,
                "tooltip": tooltip
            },
            "itemStyle": {
                "borderWidth": 1,
                "opacity": 0 if kind == "line" else 1,
            },
            "lineStyle": {kk: vv for kk, vv in linestyle.items()},
            "emphasis": {
                "itemStyle": {
                    "opacity": 1,
                    "borderColor": "black",
                    "borderWidth": 2,
                    "shadowBlur": 10,
                    "shadowColor": "yellow",
                }
            }
        }]
        ax.opts["series"][-1].update(series_kws)
    if by in cols:
        by_typ = dtypes.query("name == @by")["type"].iloc[0]
        ax.opts["visualMap"] = {
            "dimension": by,
            "realtime": True,
            "calculable": True,
            "color": ax.create_palette(cmap) if cmap else None,
            "seriesIndex": [len(ax.opts["series"]) - i - 1 for i, _ in enumerate(y)],
            "outOfRange": {
                "symbol": ["none"],
            }
        }
        if by_typ == "time":
            ax.opts["visualMap"].update(
                formatter=ax.make_datetime_formatter(ax.date_format)
            )
        elif by_typ != "ordinal":
            ax.opts["visualMap"].update(
                type="continuous",
                min=data[by].min(),
                max=data[by].max(),
            )
        else:
            ax.opts["visualMap"].update(
                type="piecewise",
                categories=data[by].unique()
                #pieces=[{"value": x, "label": f"{x}"} for x in data[by].unique()]
            )

    ax.opts["xAxis"] = {
        "type": "time" if dimensions[0]["type"] == "time" else ("value" if dimensions[0]["type"] != "ordinal" else "category"),
        "nameLocation": "center",
        "nameGap": 35,
        "scale": True,
    }
    if ax.opts["xAxis"]["type"] == "time":
        ax.opts["xAxis"]["axisLabel"] = dict(
            formatter=ax.make_datetime_formatter(fmt=date_fmt))
        ax.opts["xAxis"]["axisPointer"] = dict(label=dict(
            formatter=ax.make_datetime_formatter(fmt=date_fmt)))
        ax.opts["dataZoom"] = [{"type": "inside"}, {"type": "slider"}]

    ax.opts["yAxis"] = {
        "type": "value",
        "nameLocation": "center",
        "nameGap": 35,
        "scale": True,
    }
    ax.opts["tooltip"] = {
        "trigger": "item" if dimensions[0]["type"] != "time" else "axis"}
    if not legend:
        ax.opts.pop("legend", None)

    if xlim[0] is not None:
        ax.opts["xAxis"]["min"] = xlim[0]
    if xlim[0] is not None:
        ax.opts["xAxis"]["max"] = xlim[1]
    if ylim[0] is not None:
        ax.opts["yAxis"]["min"] = ylim[0]
    if ylim[0] is not None:
        ax.opts["yAxis"]["max"] = ylim[1]

    ax.opts["yAxis"] = [ax.opts["yAxis"]]
    ax.opts["xAxis"] = [ax.opts["xAxis"]]

    if ax.opts["xAxis"][0]["type"] == "time":
        features.append("dataZoom")

    for k, v in ax.opts["toolbox"]["feature"].items():
        v.update(show=k in features)
    if ax.opts["xAxis"][0]["type"] == "time":
        ax.add_line_zoom()

    if color is not None:
        ax.opts["color"] = color

    return ax

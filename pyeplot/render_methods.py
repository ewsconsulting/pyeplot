#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging

log = logging.getLogger(__name__)  # pylint: disable=invalid-name

TEMPLATES = {
    "rectangle": """
        var x_values = [];
        params.encode.x.forEach(function(v) {x_values.push(api.value(v))});
        var y_values = [];
        params.encode.y.forEach(function(v) {y_values.push(api.value(v))});   
        var x_start = x_values[0];
        var y_start = y_values[0];
        var x_end = x_values[x_values.length - 1];
        var y_end = y_values[y_values.length - 1];
        var start = api.coord([x_start, y_start]);
        var end = api.coord([x_end, y_end]);
        var size = api.size([x_end - x_start, y_end - y_start]);
        
        var rectShape = echarts.graphic.clipRectByRect({
            type: 'rect',
            shape: {
                x: start[0],
                y: end[1],
                width: size[0],
                height: size[1]
            }, {
            x: params.coordSys.x,
            y: params.coordSys.y,
            width: params.coordSys.width,
            height: params.coordSys.height
        });

        return {
            type: 'rect',
            shape: rectShape,
            style: api.style()
        };""",
    "categorical_rectangle": """
        var x_values = [];
        params.encode.x.forEach(function(v) {x_values.push(api.value(v))});
        var y_values = [];
        params.encode.y.forEach(function(v) {y_values.push(api.value(v))});   
        var x_start = x_values[0];
        var y_start = y_values[0];
        var x_end = x_values[x_values.length - 1];
        var y_end = y_values[y_values.length - 1];
        var start = api.coord([x_start, y_start]);
        var end = api.coord([x_end, y_end]);
        var size = api.size([x_end - x_start, y_end - y_start]);
        if (params.encode.x.length == 1) {
            var width = size[0] * {{ width }};
            var rectShape = {
                x: start[0] - width / 2,
                y: end[1],
                width: width,
                height: size[1]
            };
        } else {
            var height = size[1] * {{ width }};
            var rectShape = {
                x: start[0],
                y: end[1] - height / 2,
                width: size[0],
                height: height
            };
        };
        rectShape = echarts.graphic.clipRectByRect(rectShape, {
            x: params.coordSys.x,
            y: params.coordSys.y,
            width: params.coordSys.width,
            height: params.coordSys.height
        });
        return {
            type: 'rect',
            shape: rectShape,
            style: api.style()
        };""",
    "error_bar": """
        xValue = api.value(0)
        highPoint = api.coord([xValue, api.value(1)])
        lowPoint = api.coord([xValue, api.value(2)])
        halfWidth = api.size([1, 0])[0] * {{ width }} * 0.5
        style = api.style({
            "stroke": api.visual('color'),
            "fill": null
        })

        return {
            "type": 'group',
            "children": [{
                "type": 'line',
                "shape": {
                    "x1": highPoint[0] - halfWidth, "y1": highPoint[1],
                    "x2": highPoint[0] + halfWidth, "y2": highPoint[1]
                },
                "style": style
            }, {
                "type": 'line',
                "shape": {
                    "x1": highPoint[0], "y1": highPoint[1],
                    "x2": lowPoint[0], "y2": lowPoint[1]
                },
                "style": style
            }, {
                "type": 'line',
                "shape": {
                    "x1": lowPoint[0] - halfWidth, "y1": lowPoint[1],
                    "x2": lowPoint[0] + halfWidth, "y2": lowPoint[1]
                },
                "style": style
            }]
        };""",
    "violin": """
        categoryIndex = api.value(params.encode.x)
        width = api.size([0, 1])[0] * {{ width }}
        points_left = []
        points_right = []
        for i in range(params.encode.y.length / 2):
            x_i = params.encode.y[2 * i]
            y_i = params.encode.y[2 * i + 1]
            x = api.value(x_i)
            y = api.value(y_i)
            pt = api.coord([categoryIndex, y])
            px_r = [pt[0] - width * 0.5 * x, pt[1]]
            px_l = [pt[0] + width * 0.5 * x, pt[1]]
            points_left.push(px_l)
            points_right.push(px_r)

        points_right.reverse()
        points = points_left.concat(points_right)
        color = api.visual("color")
        return {"type": "polygon", "shape": {"points": points}, "style": api.style({"fill": color, "stroke": echarts.color.lift(color)})}
    """,
    "hbar": """       
        var x_values = [];
        params.encode.x.forEach(function(v) {x_values.push(api.value(v))});
        {% if selector_code %}
        {{ selector_code }}          
        {% else %} 
        var y_values = [params.seriesIndex];    
        {% endif %}
        var y_start = y_values[0];
        var y_end = y_values[y_values.length - 1];        
        
        var x_start = x_values[0];
        var x_end = x_values[x_values.length - 1];
        var start = api.coord([x_start, y_start]);
        var end = api.coord([x_end, y_end]);
        var size = api.size([x_end - x_start, y_end - y_start]);
        var height = size[1] * {{ width }};
        var rectShape = {
            x: start[0],
            y: end[1] - height / 2,
            width: size[0],
            height: height
        };
        rectShape = echarts.graphic.clipRectByRect(rectShape, {
            x: params.coordSys.x,
            y: params.coordSys.y,
            width: params.coordSys.width,
            height: params.coordSys.height
        });       
        return {
            type: 'rect',
            shape: rectShape,
            style: api.style( {%- if style -%}{{ style }}{%- endif -%} )
        }"""
}

class RenderItemFactory:

    JS_VAR_NAME = "renderItem_{{ chart.chart_id }}"
    FUNC_DEF = "function %s(params, api)" % JS_VAR_NAME
    TEMPLATES = TEMPLATES

    @classmethod
    def _make(cls, ax, key, **kws):
        return ax.add_js_variable(creation_code=cls.FUNC_DEF + "{" + cls.TEMPLATES[key] + "};", alias_code=cls.JS_VAR_NAME, **kws)

    @classmethod
    def make_hbar(cls, ax, width=1.0, style="", selector_code=None):
        return cls._make(ax, "hbar", width=width, style=style, selector_code=selector_code)

    @classmethod
    def make_rectangle(cls, ax):
        return cls._make(ax, "rectangle")

    @classmethod
    def make_categorical_rectangle(cls, ax, width=1.0):
        return cls._make(ax, "categorical_rectangle", width=width)

    @classmethod
    def make_error_bar(cls, ax, width=1.0):
        return cls._make(ax, "error_bar", width=width)

    @classmethod
    def make_violin(cls, ax, width=1.0):
        return cls._make(ax, "violin", width=width)

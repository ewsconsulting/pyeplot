import pathlib

from setuptools import find_packages, setup

[p.unlink() for p in pathlib.Path('./pyeplot').rglob('*.py[co]')]
[p.rmdir() for p in pathlib.Path('./pyeplot').rglob('__pycache__')]

setup(
    name="pyeplot",
    version="0.0.1",
    author="Fabien Farella",
    author_email="f.farella@ews-consulting.at",
    description="A wrapper around pyecharts",
    packages=find_packages(),
    include_package_data=True,
    install_requires=['pyecharts==0.5.11', 'rjsmin==1.1.0', 'Jinja2', 'numpy', 'pandas','bokeh', 'matplotlib', 'marshmallow>=3',
                      'matplotlib', 'jupyter-echarts-pypkg==0.1.2', 'jupyter-echarts-pypkg==0.1.2', 'ipython'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
